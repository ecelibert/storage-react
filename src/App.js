import { useState, useEffect } from "react";
import Todo from "./components/todo";

const App = () => {
  const [idCounter, setIdCounter] = useState(1);
  const [todos, setTodos] = useState([]);
  const [newTodo, setNewTodo] = useState({
    id: 1,
    text: "",
    done: false,
    editing: false,
    editText: "",
  });
  const [loaded, setLoaded] = useState(false);

  // Load localstorage data on mount
  useEffect(() => {
    if (localStorage.hasOwnProperty("todos")) {
      const todoStorage = JSON.parse(localStorage.getItem("todos"));
      setTodos(todoStorage);

      // Get the current id
      if (todoStorage.length) {
        const currentId = Math.max(...todoStorage.map((todo) => todo.id));
        setIdCounter(currentId + 1);
        setNewTodo((previousState) => {
          return { ...previousState, id: currentId + 1 };
        });
      }
    }
    setLoaded(true);
  }, []);

  // Update the items in localstorage
  useEffect(() => {
    if (loaded) localStorage.setItem("todos", JSON.stringify(todos));
  }, [todos, loaded]);

  // Manage the filters
  const [activeFilter, setactiveFilter] = useState("all");
  const [filteredTodos, setFilteredTodos] = useState([]);

  const onNewTodoChange = (event) => {
    const newTodoText = event.target.value;
    setNewTodo((previousState) => {
      return { ...previousState, text: newTodoText };
    });
  };

  const addTodo = () => {
    if (newTodo.text.trim().length) {
      setTodos((previousState) => [...previousState, newTodo]);

      setIdCounter((idCounter) => {
        return idCounter + 1;
      });

      setNewTodo((previousState) => {
        return { ...previousState, id: idCounter + 1, text: "" };
      });
    }
  };

  const handleButtons = (action = "", idTodo = 0) => {
    const indexTodo = todos.map((todo) => todo.id).indexOf(idTodo);
    const todosCopy = [...todos];

    if (indexTodo !== -1 || action === "deleteDone" || action === "deleteAll") {
      switch (action) {
        case "done":
          todosCopy[indexTodo].done = !todosCopy[indexTodo].done;
          setTodos(todosCopy);
          break;

        case "editing":
          todosCopy[indexTodo].editing = true;
          todosCopy[indexTodo].editText = todosCopy[indexTodo].text;
          setTodos(todosCopy);
          break;

        case "cancelEdit":
          todosCopy[indexTodo].editing = false;
          setTodos(todosCopy);
          break;

        case "saveEdit":
          if (todosCopy[indexTodo].editText.trim().length) {
            todosCopy[indexTodo].editing = false;
            todosCopy[indexTodo].text = todosCopy[indexTodo].editText;
            setTodos(todosCopy);
          }
          break;

        case "delete":
          const todosFilteredDeleted = todos.filter(
            (todo) => todo.id !== idTodo
          );
          setTodos(todosFilteredDeleted);
          break;

        case "deleteDone":
          const todosFilteredDone = todos.filter((todo) => todo.done === false);
          setTodos(todosFilteredDone);
          break;

        case "deleteAll":
          setTodos([]);
          break;

        default:
          break;
      }
    }
  };

  const onEditTodoChange = (event) => {
    const idTodo = parseInt(event.target.id);
    const todoText = event.target.value;

    const indexTodo = todos.map((todo) => todo.id).indexOf(idTodo);
    const todosCopy = [...todos];

    if (indexTodo !== -1) {
      todosCopy[indexTodo].editText = todoText;
      setTodos(todosCopy);
    }
  };

  useEffect(() => {
    const newFilteredTodos = todos.filter((todo) => {
      if (activeFilter === "all") return true;
      if (activeFilter === "done") return todo.done === true;
      if (activeFilter === "todo") return todo.done === false;
      return false;
    });

    setFilteredTodos(newFilteredTodos);
  }, [todos, activeFilter]);

  return (
    <div className="App">
      <div className="container my-4">
        <h1 className="text-center fw-bold mb-4">PWA React</h1>

        <div className="row border border-secondary p-4 rounded">
          <div className="input-group mb-3">
            <div className="input-group-text bg-info">
              <i className="bi bi-journals"></i>
            </div>
            <input
              className="form-control"
              type="text"
              placeholder="Enter text..."
              value={newTodo.text}
              onChange={onNewTodoChange}
            />
          </div>
          <button className="btn btn-info" onClick={() => addTodo()}>
            Add stuff to do
          </button>
        </div>

        {todos.length ? (
          <div className="my-4">
            <h3 className="text-center">Todo List</h3>

            <div className="row">
              <div className="col-12">
                <button
                  className={
                    "col-4 btn btn-sm " +
                    (activeFilter === "all"
                      ? "btn-outline-info disabled"
                      : "btn-info")
                  }
                  onClick={() => setactiveFilter("all")}
                >
                  All
                </button>
                <button
                  className={
                    "col-4 btn btn-sm " +
                    (activeFilter === "done"
                      ? "btn-outline-info disabled"
                      : "btn-info")
                  }
                  onClick={() => setactiveFilter("done")}
                >
                  Done
                </button>
                <button
                  className={
                    "col-4 btn btn-sm " +
                    (activeFilter === "todo"
                      ? "btn-outline-info disabled"
                      : "btn-info")
                  }
                  onClick={() => setactiveFilter("todo")}
                >
                  Todo
                </button>
              </div>
            </div>

            {filteredTodos.map((todo, index) => (
              <Todo
                key={index}
                todo={todo}
                onEditTodoChange={onEditTodoChange}
                handleButtons={handleButtons}
              />
            ))}

            <div className="row mt-4">
              <div className="col-12">
                <button
                  className={"btn btn-sm btn-danger col-6"}
                  onClick={() => handleButtons("deleteDone")}
                >
                  Delete done tasks
                </button>
                <button
                  className={"btn btn-sm btn-danger col-6"}
                  onClick={() => handleButtons("deleteAll")}
                >
                  Delete all tasks
                </button>
              </div>
            </div>
          </div>
        ) : (
          <h4 className="my-4 text-center">
            No todo is currently on the list. Try adding one !
          </h4>
        )}
      </div>
    </div>
  );
};

export default App;
