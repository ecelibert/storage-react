const Todo = ({ todo, onEditTodoChange, handleButtons }) => {
  return (
    <div className="my-3 px-4 py-2 rounded border border-dark">
      {todo.editing ? (
        <div className="d-flex justify-content-between align-items-center">
          <input
            className="form-control form-control-sm border border-info me-2"
            id={todo.id}
            type="text"
            placeholder="Enter text..."
            value={todo.editText}
            onChange={onEditTodoChange}
          />

          <button
            className="btn btn-sm btn-danger me-2"
            onClick={() => handleButtons("cancelEdit", todo.id)}
          >
            Cancel
          </button>
          <button
            className="btn btn-sm btn-success"
            onClick={() => handleButtons("saveEdit", todo.id)}
          >
            Save
          </button>
        </div>
      ) : (
        <div className="d-flex justify-content-between align-items-center">
          <span
            className={
              "text-break fw-bold " +
              (todo.done ? "text-decoration-line-through text-danger " : "")
            }
          >
            {todo.text}
          </span>

          <span>
            <i
              className="bi bi-check-square-fill text-success"
              style={{ cursor: "pointer" }}
              onClick={() => handleButtons("done", todo.id)}
            ></i>

            <i
              className="bi bi-pencil-fill text-warning mx-3"
              style={{ cursor: "pointer" }}
              onClick={() => handleButtons("editing", todo.id)}
            ></i>

            <i
              className="bi bi-trash-fill text-danger"
              style={{ cursor: "pointer" }}
              onClick={() => handleButtons("delete", todo.id)}
            ></i>
          </span>
        </div>
      )}
    </div>
  );
};

export default Todo;
